#! /bin/python

## Author: Dan Johansen <strit@manjaro.org> ##

import sys
import subprocess
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QDesktopWidget, QPushButton, QAction, QLineEdit, QMessageBox, QLabel, QComboBox, QFileDialog
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot
import re
import time
from blkinfo import BlkDiskInfo

class App(QMainWindow):

    def __init__(self):
        super().__init__()
        self.initUI()
        
    def initUI(self):
        self.resize(640, 720)
        self.center()
        self.setWindowTitle('Manjaro ARM Installer GUI')
        self.setWindowIcon(QIcon('icon.png')) #Does not work on wayland it seems

        self.statusBar().showMessage('Version 0.1 Alpha')
        
        # Menus
        mainMenu = self.menuBar()
        fileMenu = mainMenu.addMenu('File')
        fileMenu.addAction('Close').triggered.connect(self.on_click_close)
        toolsMenu = mainMenu.addMenu('Tools')
        toolsMenu.addAction('Get Profiles').triggered.connect(self.getarmprofiles)
        
        # Labels
        username = QLabel('Username:', self)
        username.move(20, 20)
        usergroups = QLabel('User Groups:', self)
        usergroups.move(20, 60)
        fullname = QLabel('Full name:', self)
        fullname.move(20, 100)
        password = QLabel('Password:', self)
        password.move(20, 140)
        passwordconf = QLabel('Confirm Password:', self)
        passwordconf.move(20, 180)
        rootpassword = QLabel('Root Password:', self)
        rootpassword.move(20, 220)
        rootpasswordconf = QLabel('Confirm Root Password:', self)
        rootpasswordconf.move(20, 260)
        device = QLabel('Device:', self)
        device.move(20, 300)
        edition = QLabel('Edition:', self)
        edition.move(20, 340)
        target = QLabel('SD/eMMC Card:', self)
        target.move(20, 380)
        timezone = QLabel('Timezone:', self)
        timezone.move(20, 420)
        locale = QLabel('Locale:', self)
        locale.move(20, 460)
        keyboard = QLabel('Key Layout:', self)
        keyboard.move(20, 500)
        keyboardcli = QLabel('Key Layout (CLI):', self)
        keyboardcli.move(20, 540)
        hostname = QLabel('Hostname:', self)
        hostname.move(20, 580)
        
        # Probe for drives without mounted partitions
        disks_info = BlkDiskInfo()
        filters = {
            'is_mounted': False
        }
        unmounted_disks = disks_info.get_disks(filters)
        disks = [ info[ 'name' ] for info in unmounted_disks ]
        
        ## Text input field
        # Username (must be all lowercase)
        self.username = QLineEdit(self)
        self.username.move(150, 20)
        self.username.resize(280, 30)
        #User groups (comma separated list, empty if none)
        # (default: wheel,sys,input,video,storage,lp,network,users,power)
        self.usergroups = QLineEdit(self)
        self.usergroups.move(150, 60)
        self.usergroups.resize(280, 30)
        # Full name
        self.fullname = QLineEdit(self)
        self.fullname.move(150, 100)
        self.fullname.resize(280, 30)
        # Password
        self.password = QLineEdit(self)
        self.password.setEchoMode(QLineEdit.Password)
        self.password.move(150, 140)
        self.password.resize(280, 30)
        # Confirm Password
        self.passwordconf = QLineEdit(self)
        self.passwordconf.setEchoMode(QLineEdit.Password)
        self.passwordconf.move(150, 180)
        self.passwordconf.resize(280, 30)
        # Root Password
        self.rootpassword = QLineEdit(self)
        self.rootpassword.setEchoMode(QLineEdit.Password)
        self.rootpassword.move(150, 220)
        self.rootpassword.resize(280, 30)
        # Confirm Root Password
        self.rootpasswordconf = QLineEdit(self)
        self.rootpasswordconf.setEchoMode(QLineEdit.Password)
        self.rootpasswordconf.move(150, 260)
        self.rootpasswordconf.resize(280, 30)
        # Hostname
        self.hostname = QLineEdit(self)
        self.hostname.move(150, 580)
        self.hostname.resize(280, 30)
        
        ## Combo boxes
        # Device
        self.devicebox = QComboBox(self)
        self.devicebox.addItem('Raspberry Pi 4', 'rpi4')
        self.devicebox.addItem('Pinebook Pro', 'pbpro')
        self.devicebox.addItem('Rock Pi 4', 'rockpi4')
        self.devicebox.addItem('RockPro64', 'rockpro64')
        self.devicebox.move(150, 300)
        self.devicebox.resize(125, 30)
        # Edition
        self.editionbox = QComboBox(self)
        self.editionbox.addItem('Xfce', 'xfce')
        self.editionbox.addItem('KDE Plasma', 'kde-plasma')
        self.editionbox.addItem('Minimal', 'minimal')
        self.editionbox.addItem('Mate', 'mate')
        self.editionbox.move(150, 340)
        self.editionbox.resize(125, 30)
        # Target device SD or eMMC
        self.targetbox = QComboBox(self)
        self.targetbox.addItems( disks )
        self.targetbox.move(150, 380)
        self.targetbox.resize(125, 30)
        # Locale
        self.localebox = QComboBox(self)
        self.localebox.addItem('Dansk', 'da_DK.UTF-8')
        self.localebox.addItem('English (US)', 'en_US.UTF-8')
        self.localebox.addItem('English (UK)', 'en_GB.UTF-8')
        self.localebox.move(150, 460)
        self.localebox.resize(125, 30)
        # Keybaord layout
        self.keyboardbox = QComboBox(self)
        self.keyboardbox.addItem('Dansk', 'dk')
        self.keyboardbox.addItem('English (US)', 'us')
        self.keyboardbox.addItem('English (UK)', 'gb')
        self.keyboardbox.move(150, 500)
        self.keyboardbox.resize(125, 30)
        
        # Button in the window
        buttonClose = QPushButton('Close', self)
        buttonClose.setToolTip('Closes the Installer script and echos the variables in terminal!')
        buttonClose.move(100, 620)
        buttonClose.clicked.connect(self.on_click_close)

        # Button to test input
        buttonTest = QPushButton('Test', self)
        buttonTest.setToolTip('Tests input field variables for validity')
        buttonTest.move(200, 620)
        buttonTest.clicked.connect(self.on_click_test)

        # Button to install to SD
        buttonInstall = QPushButton('Install', self)
        buttonInstall.setToolTip('Runs the installation . . . theoretically')
        buttonInstall.move(300, 620)
        buttonInstall.clicked.connect(self.on_click_install)

        self.show()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def on_click_test(self):
        usernameValue = self.username.text()
        usergroupsValue = self.usergroups.text().split(',')
        passwordValue = self.password.text()
        rootpasswordValue = self.rootpassword.text()
        hostnameValue = self.hostname.text()
        if passwordValue == self.passwordconf.text():
            passwordconfValue = True
        else:
            passwordconfValue = False
        rootpasswordValue = self.rootpassword.text()
        if rootpasswordValue == self.rootpasswordconf.text():
            rootpasswordconfValue = True
        else:
            rootpasswordconfValue = False
        if re.match('^[a-z]+$', usernameValue):
            print('Username OK (only lowercase)')
        else:
            print('Username not OK (blank, or contains uppercase or symbols)')
        if hostnameValue != '':
            print('Hostname OK (not blank)')
        else:
            print('Hostname not OK (blank)')
        print("Password match? = " + str(passwordconfValue))
        print("Root Password match? = " + str(rootpasswordconfValue))
        print("Additional user groups:")
        for i in usergroupsValue:
            print(i)

    @pyqtSlot()
    def on_click_close(self):
        usernameValue = self.username.text()
        usergroupsValue = self.usergroups.text()
        fullnameValue = self.fullname.text()
        passwordValue = self.password.text()
        passwordconfValue = self.passwordconf.text()
        rootpasswordValue = self.rootpassword.text()
        rootpasswordconfValue = self.rootpasswordconf.text()
        deviceValue = self.devicebox.currentData()
        editionValue = self.editionbox.currentData()
        targetValue = self.targetbox.currentText()
        localeValue = self.localebox.currentData()
        keyboardValue = self.keyboardbox.currentData()
        hostnameValue = self.hostname.text()
        print("Username = " + usernameValue)
        print("User groups = " + usergroupsValue)
        print("Full name = " + fullnameValue)
        print("Password for " + usernameValue + " = " + passwordValue)
        print("Password confirm for " + usernameValue + " = " + passwordconfValue)
        print("Password for root = " + rootpasswordValue)
        print("Password confirm for root = " + rootpasswordconfValue)
        print("Device = " + deviceValue)
        print("Edition = " + editionValue)
        print("SD/eMMC = " + targetValue)
        print("Locale = " + localeValue)
        print("Keyboard Layout = " + keyboardValue)
        print("Hostname = " + hostnameValue)
        #subprocess.Popen(["echo", f"Username = {usernameValue}"])
        #subprocess.Popen(["echo", f"Password for {usernameValue} = {passwordValue}"])
        #subprocess.Popen(["echo", f"Password for root = {rootpasswordValue}"])
        #subprocess.Popen(["echo", f"Device = {deviceValue}"])
        #subprocess.Popen(["echo", f"Edition = {editionValue}"])
        #subprocess.Popen(["echo", f"SD/eMMC = {targetValue}"])
        #subprocess.Popen(["echo", f"Locale = {localeValue}"])
        #subprocess.Popen(["echo", f"Keyboard Layout = {keyboardValue}"])
        #subprocess.Popen(["echo", f"Hostname = {hostnameValue}"])
        sys.exit()

    def on_click_install(self):
        startTime = time.time()

        self.getarmprofiles()
        self.prepare_card()
        self.create_install()
        self.cleanup()

        endTime = time.time()

        print("Time elapsed: " + str(endTime - startTime))
        
    def on_click_getarmprofiles(self):
        self.getarmprofiles()

    def getarmprofiles(self):
        print("getarmprofiles called")
        time.sleep(1)

    def prepare_card(self):
        print("prepare_card called")
        time.sleep(1)

    def create_install(self):
        print("create_install called")
        time.sleep(1)

    def cleanup(self):
        print("cleanup called")
        time.sleep(1)
        
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_()) 
